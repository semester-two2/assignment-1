#include<stdio.h>
#include<math.h>
int main()
{
    int a, b, c, root;
    printf("Enter a: ");
    scanf("%d", &a);
    printf("Enter b: ");
    scanf("%d", &b);
    printf("Enter c: ");
    scanf("%d", &c);

    root = b * b - 4 * a * c;

    if(root <= 0)
        printf("complex");
    else if (a == 0)
        printf("Invalid input");
    else
        printf("\nAnswer: %d", root);

    return 0;
}
